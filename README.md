# Expreso PHP y la vía correcta

## Exordio

Este repositorio contiene los archivos del libro «Expreso PHP y la vía correcta».
Este texto se compone de una compilación entre material escrito por mi y traducciones *libres* (taambién hechas por mi) a partir del conocido "*PHP: The Right Way*". Por tanto este texto responde más a una *adaptación y ampliación* que a una traducción, propiamente dicho.

El repositorio original de *PHP: The Right Way* corresponde a un proyecto para *GitHub Pages* con los contenidos escritos en formato markdown.

Pero yo tengo preferencia por el [reStructuredText soportado por Sphinx](http://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html), [GitLab](https://gitlab.com/), y [Read The Docs](https://readthedocs.org/); así que mi *adaptación* se estructura completamente diferente a *PHP: The Right Way*, pero manteniendo mucho de sus contenidos, modificando otro tanto y agregando unos nuevos (y conservando la misma licencia). En este repositorio cada sección corresponde con un archivo `{xx}_{id}.rst` dónde `xx` es el número de sección y `id` una cadena que lo identifica.

## Contale al mundo

Seguro que este texto le sirve a mucho(a)s nuevos desarrolladore(a)s que no se llevan todavía muy bien con el inglés, con tu apoyo podés hacer llegar este texto a más gente y ayudar a nuevo(a)s desarrolladore(a)s a saber donde encontrar información útil.

## Cómo contribuir

El camino típico:

1. [Creá una bifurcación](https://docs.gitlab.com/ce/gitlab-basics/fork-project.html) (*fork*).
2. [Opcionalmente podés instalar Sphinx](http://www.sphinx-doc.org/en/master/usage/installation.html)  para previsualizar tus cambios en local.
3. [Ejecutar una solicitud de mezca](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) (*merge request*).

### brevísima guía de estilo para el contribuyente

1. Usá castellano neutro.
2. Traducí "*you*" como «*vos*».
3. Preferí las comillas castellanas («») sobre las inglesas ("") para envolver prosa.
4. Usá cuatro (4) espacios para identar el texto, no usés tabulaciones.
5. El código de ejemplo debe apegarse a la recomendación PSR-1 o superior.

## ¿Por qué?

Tras acumular unos cuantos años de experiencia, y ver las mil y us pseudoprofecías del fin de PHP, resulta que ya vamos por la versión 8 y continuando. Cuando comencé este proyecto mi idea era simplemente tratar de mantener vigente una versión en español de *PHP: The Right Way* (objetivo no cumplido), pero a la postre he encontrado que no solo quería tradecir el texto, sino que quería hacer mis modificaciones en función de mi propia experiencia. A mi juicio, esto resultariá ya no solo en texto aportado a la comunidad, sino también en una herramienta para volcar mis propias reflexiones y experiencias. Digamos que se ha volcado en un proyecto un poco más personal, pero que no deja de fundamentarse en *PHP: The Right Way*.

También he de señalar que entre los angloparlantes ya es una referencia canon el [proyecto original](http://phptherightway.com/), pero su [traducción al español](http://phpdevenezuela.github.io/php-the-right-way/) se quedó rezagada a finales de 2016 y la versión de PHP de referencia para la misma es la **5.6**. Es cierto que podría haber hecho una *bifurcación* del repositorio de esa traducción, pero como dije antes, dejé de simplemente traducir el texto y pasé a mezclarle composición propia, además de mi preferencia por reStructuredText y GitLab, así que como resultado he decidido llevar el proyecto así y aquí en lugar de asá y allá.

## ¿Quién?

Mi nombre es [Yeshua Rodas](https://gitlab.com/yybalam), estudio Filosofía y soy desarrollador de planta para la UPNFM. Me considero una suerte de «programador minimalista», autor del proyecto Xibalbá, y en mi tiempo libre, a veces, me gusta escribir textos técnicos, justo como este.

## Licencia

[Reconocimiento-NoComercial-CompartirIgual 3.0 Unported (CC BF-NC-SA 3.0)](https://creativecommons.org/licenses/by-nc-sa/3.0/deed.es_ES)
