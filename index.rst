
.. title:: Expreso PHP y la vía correcta

=============================
Expreso PHP y la vía correcta
=============================

.. warning::
  Este libro es un *trabajo en progreso* en el que vuelco, más que una traducción, una adaptación al español de *PHP: The Right Way* junto con contenido propio conforme a la experiencia que he acumulado tras años de desarrollo de *software* en PHP, con la intención (o esperanza) de que este texto pueda servir de referencia sobre como programar *correctamente* con PHP. Al haber comenzado este texto como una *traducción libre* de *PHP: The Right Way*, conserva muchas partes que, en efecto, eso son, pero también hay otras que, entendidas como *adaptaciones* y *modificaciones*, se distancian del texto original en inglés. Por ahora, aproximadamente he *terminado* (y actualizado) 1/3 del libro, y si se compara con *PHP The Right Way* se verá que **Expreso PHP y la vía correcta** aunque versa sobre la misma idea, se aleja en forma y estructura de aquel, constituyéndose como una nueva obra en sí misma.

Tabla de contenido
******************

.. toctree::
   :maxdepth: 2

   pages/00_libro
   pages/01_intro
   pages/02_aspectos_php
   pages/03_poo
   pages/04_gestion_dependencias
   pages/05_guia_estilo
   pages/06_buenas_practicas
