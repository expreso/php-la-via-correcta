Expreso PHP y La vía correcta
*****************************

PHP es un lenguaje de programación potente, flexible y dinámico, de código abierto, diseñado principalmente para el *desarrollo Web* y que ha alcanzado una tremenda popularidad. Hoy por hoy muchísimos sitios, portales y aplicaciones Web funcionan con PHP, y cada día nuevos proyectos se desarrollan con este lenguaje.

No obstante, por Internet pulula un montón de información desfasada sobre PHP que lleva a lo(a)s nuevo(a)s desarrolladore(a)s por falsas sendas, propagando malas prácticas de programación  y código inseguro. **Expreso PHP y la vía correcta** es una referencia ligera *fácil de leer* para aprender a programar con este lenguaje (y también a modo de referencia); expone las convenciones más populares de programación en PHP, provee enlaces a tutoriales y material de referencia de calidad que ronda por Internet, así como se presentan algunas de las mejores prácticas que la comunidad de contribuyentes han considerado a la fecha.

*No existe una forma canónica para programar con PHP* propiamente dicho. Este texto pretende guiar a lo(a)s nuevo(a)s desarrolladore(a)s de PHP hacia temas que podrían no descubrir hasta que es muy tarde, así como también pretende dar a los profesionales ideas frescas sobre esos temas que han estado trabajando por años sin reconsiderarlos. Este texto expone sugerencias sobre herramientas a utilizar de entre la amplia variedad que existe, explicando las diferencias de enfoque y casos de uso, sin mayor preferencia de alguna sobre otra.

Este es un documento vivo y continuará actualizándose con más información útil y ejemplos en la medida que el tiempo me lo permita.

El libro
========

Como he indicado en la advertencia del índice, comencé a redactar este texto como una traducción de *PHP: The Right Way*, pero a medida que lo traducía me encontraba con más y más cosas que me apetecían cambiar, explicar de manera distinta, cambiar de orden, incluso agregar otros temas. Al final he terminado haciendo una versión que **no** es una traducción de *PHP: The Right Way*, aunque sí una adaptación que creo es fiel a la idea del original, pero con adaptaciones, modificaciones y agregados que, según mi experiencia e interés, dan al lector hispanohablante un texto de referencia no solo tan rico como lo es *PHP: The Right Way* para el angloparlante, sino también más fluído y acorde a nuestra forma de lectura y de entender las cosas por la idiosincracias de nuestra lengua. Si el lector examina y compara este texto con el original *PHP: The Right Way* notará que el presente **Expreso PHP y la vía correcta** difiere en el orden de algunos capítulos, en la explicación de muchos temas, y que es un poco más amplio que *PHP: The Right Way*. Mientras que *PHP: The Right Way* se enfoca en ser un *sitio de consulta y referencia*, **Expreso PHP y la vía correcta** lo he gestado como un libro completo que además de fungir como consulta y referencia sirva también por sí mismo como texto de introducción a la programación en general y PHP en particular.

De hecho, *PHP: The Right Way* (y sus referencias) no han sido mis únicas fuentes, y he bebido también de otros textos, como *PHP 5 Power Programing*, *Eloquent JavaScript*, entre otros.

¡Corré la voz!
--------------

La versión más reciente de **Expreso PHP y la vía correcta** está siempre disponible en línea en ReadTheDocs, si este texto te ha gustado y resulta útil, entonces que maravilla recibir tu apoyo ayudando a que otro(a)s desarrolladore(a)s de PHP (nuevos o no) puedan encontrar información más útil sobre este lenguaje, y si querés ayudar aún más, te invito a adquirir copias (por un precio simbólico) en los formatos PDF, EPUB y/o MOBI vía Leanpub.
