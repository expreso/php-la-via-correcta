Introducción
************

PHP es un lenguaje interpretado, es decir, los programas escritos con PHP son ejecutados por un *intérprete*, dicho intérprete debe estar instalado siempre en la máquina dónde se vaya ejecutar programas PHP, a diferencia de los lenguajes *compilados*, como C o Rust, en los cuales una vez compilado el programa para una plataforma, tal programa es capaz de ejecutarse por sí mismo.

El Grupo PHP (`PHP Group`_), la comunidad abierta detrás del desarrollo del lenguaje, ha definido la especificación_ del lenguaje de tal manera que las diferentes *implementaciones* interpreten el mismo código PHP, aunque muchas veces las implementaciones introducen particularidades propias cambiando algunos aspectos respecto a la especificación.

A los *intérpretes* también se les llama **máquinas virtuales** o **motores** (*engines*), que son el resultado del desarrollo de una implementación concreta. En el caso de PHP, la implpmentación más extendida (y por mucho) es el motor *Zend Engine*, desarrollado en *C* por Zend Technologies y el Equipo de PHP.

En adelante en este texto, *PHP* referirá tanto al lenguaje (su especificación) como al *Zend Engine* (su implementación), y por tanto las versiones dadas, por ejemplo *PHP 8.3*, hace referencia a *la implementación de PHP 8.3 en Zend Engine*, que a todas luces resulta tremendamente largo e impráctico de escribir, decir y citar, cuando con «PHP 8.3» basta para que quede claro.

Usá la última versión estable (8.3)
===================================

Si estás comenzando con PHP, empezá utilizando la última versión estable disponible: `PHP 8.3`_. El motor fue ampliamente reescrito y optimizado, con muchas nuevas caractecrísticas que implican considerables mejoras respecto de las versiones anteriores.

Es recomendable emplear siempre una versión de PHP actualizada. La versión anterior, PHP 7.4,está por llegar al final de su `periodo de soporte <https://www.php.net/supported-versions.php>`_. Hoy por hoy es sencillo realizar las actualizaciones, y hay pocos cambios que no sean retrocompatibles. Podés revisarlos para todas las versiones 8.x:

* `PHP 8.0 <https://www.php.net/manual/es/migration80.incompatible.php>`_
* `PHP 8.1 <https://www.php.net/manual/es/migration81.incompatible.php>`_
* `PHP 8.2 <https://www.php.net/manual/es/migration82.incompatible.php>`_
* `PHP 8.3 <https://www.php.net/manual/es/migration83.incompatible.php>`_

Si no estás seguro(a) de a que versión corresponde una funcionalidad o característica, lo podés verificar en la `documentación de PHP`_.

Servidor web interno
====================

A partir de PHP 5.3, podés empezar a aprender PHP sin necesidad de instalar y configurar un servidor web dedicado.
Basta con el servidor interno, para iniciarlo ejecutá el siguiente comando desde la terminal en la raíz de tu proyecto web:

.. code-block:: console

  php -S localhost:8000

* `Aprendé más sobre el servidor interno <http://php.net/features.commandline.webserver>`_.

Instalación en Linux
====================

Las Distribuciones Linux más populares incluyen por defecto alguna versión de PHP en sus repositorios, aunque, por lo general, tales versiones están un poco atrasadas respecto a la última versión estable. Existen varias alternativas para instalar versiones recientes de PHP en las Distribuciones. En sistemas basados en Ubuntu y Debian, por ejemplo, la mejor alternativa es proveída y mantenida por `Ondřej Surý <https://deb.sury.org/>`_ mediante su PPA.


Instalar PHP en Ubuntu (22.04) desde los repositorios regulares
---------------------------------------------------------------

.. code-block:: console

  sudo apt install php8.1-cli

Instalar PHP en Ubuntu (20.04 y 22.04) vía el PPA de Ondřej
-----------------------------------------------------------

.. code-block:: console

  sudo add-apt-repository ppa:ondrej/php
  sudo apt update
  sudo apt install php8.3-cli

Instalación en Mac
==================

Los sistemas OS X incluyen una versión de PHP preempaquetada que, por lo regular, se encuentra un poco atrasada respecto a la última versión estable.

Existen varias alternativas para instalar PHP en sistemas OS X.

Instalar PHP vía *Homebrew*
---------------------------

`Homebrew <http://brew.sh/>`_ es un potente gestor de paquetes para OS X, y puede ayudarte a instalar PHP y diversas extensiones fácilmente.
`Homebrew PHP <https://github.com/Homebrew/homebrew-php#installation>`_ es un repositorio que contiene «*formulae*» relacionado con PHP para *Homebrew*, y te permitirá instalar PHP.

En este punto, podés instalar ``php8.3``, ``php8.2``, ``php8.1``, ``php8.0`` y ``php7.4``. Por ejemplo, para instalar la última versión disponible podés ejecutar el comando:

.. code-block:: console

  brew install php@8.3

Además podés cambiar entre versiones modificando la variable ``PATH``. Alternativamente padés usar `brew-php-switcher <https://github.com/philcook/brew-php-switcher>`_ para que cambie la versión automáticamente por vos.

Instalar PHP vía *Macports*
---------------------------

El proyecto `MacPorts <https://www.macports.org/install.php>`_ es un sistema de código abierto, comunitario, diseñado para una compilación, instalación y actualización sencilla de *software* de código abierto para línea de comandos, X11 o *Aqua* en MacOS.

MacPorts soporta binarios precompilados, por lo que el usuario final no necesita recompilar las dependencias de los paquetes fuente, lo que facilita la vida si no se tienen ningún paquete instalado en el sistema.

A la fecha, padés instalar ``php54``, ``php55``, ``php56``, ``php70``, ``php71``, ``php72``, ``php73``, ``php74``, ``php80``, ``php81``, ``php82`` y ``php83`` utilizando el comando ``port install``, por ejemplo:

.. code-block:: console

  sudo port install php74
  sudo port install php83


Y luego podés ejecutar el comando ``select`` para cambiar la versión activa de PHP:

.. code-block:: console

  sudo port select --set php php83

Instalar PHP vía *phpbrew*
--------------------------

`phpbrew <https://github.com/philcook/brew-php-switcher>`_ es una herramienta para instalar y manejar varias versiones de PHP. esto puede ser realmente útil si dos aplicaciones/proyectos necesitan diferentes versiones de PHP y no utilizás máquinas virtuales.

Instaladores todo en uno
------------------------

Las soluciones previas funcionan para PHP per se y no proveen cosas como Servidores Web (Apache, Nginx, etc) ni gestores de bases de datos (MariaDB, PostreSQL, etc). Las soluciones «todo en uno» como MAMP_ y XAMPP_ instalarán todos esos programas por vos y lo mantendrán todo junto, pero esas instalaciones sencillas implican menor flexibilidad (por ejemplo, XAMPP te instala Apache y MariaDB; si quisieras NginX y PostreSQL, pues igual tendrías que instalarlos manualmente).

Instalación en Windows
======================

Podés descargar los binarios directamente de `windows.php.net/download <http://windows.php.net/download/>`_. Tras la extracción de PHP, se recomienda establecer el `PATH <http://www.windows-commandline.com/set-path-command-line/>`_ a la raíz de la carpeta de PHP (allí dónde ``php.exe`` se encuentra) y así podés ejecutar PHP desde cualquier parte.

Para aprendizaje y desarrollo local, podés usar el servidor interno que se incluye desde PHP 5.4+, así que no necesitás preocuparte por configurar nada. Si prefirieras una solución «todo en uno» que incluya un servidor web dedicado y un gestor de base de datos, entonces herramientas como `Web Platform Installer <https://www.microsoft.com/web/downloads/platform.aspx>`_, XAMPP_, EasyPHP_, OpenServer_ y WAMP_ ayuadarán a preparar rápidamente entornos de desarrollo sobre Windows. Dicho esto, esas herramientas serán ligeramente diferente de los entornos de producción, así que tené cuidado de las diferencias de entornos si estás desarrollando en Windows y desplegando en Linux.

Si necesitás ejecutar un entorno de preducción en Windows, entonces IIS7 te dará el mejor rendimiento y estabilidad. Podés utilizar `phpmanager <http://phpmanager.codeplex.com/>`_ (un *plugin* de IU para IIS7) para configurar y gestionar PHP de manera sencilla. IIS7 provee *FastCGI* por defecto y listo para usar, sólo necesitás configurar PHP como manejador. Para soporte y recursos adicionales está disponible una sección dedicada en `iis.net <http://php.iis.net/>`_ para PHP.

Generalmente ejecutar una aplicación en diferentes entornos para desarrollo y producción pueden llevar a errores extraños apareciendo en la vida real. Si estás desarrollando en Windows y desplegando en Linux (o cualquier otro sistema no Windows) entonces deberías considerar utilizar una Máquina Virtual.

Chris Tankersley tiene un artículo muy útil publicado en su bitácora en el cual expone que `herramientas usa <http://ctankersley.com/2016/11/13/developing-on-windows-2016/>`_.

.. _PHP Group: http://php.net/credits.php
.. _especificación: https://github.com/php/php-langspec
.. _PHP 8.3: http://php.net/downloads.php
.. _PHP 5.6: http://php.net/supported-versions.php
.. _documentación de PHP: http://php.net/manual/
.. _XAMPP: http://www.apachefriends.org/en/xampp.html
.. _EasyPHP: http://www.easyphp.org/
.. _OpenServer: http://open-server.ru/
.. _WAMP: http://www.wampserver.com/en/
