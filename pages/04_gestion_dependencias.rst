Gestión de dependencias
***********************

la comunidad de PHP es basta y cuenta con miles de bibliotecas,*frameworks* y componentes para escoger. Seguramente tu proyecto terminará utilizando muchas de esas bibliotecas, y por tanto tu preyecto *dependerá* de ellas, por eso las llamamos *dependencias*. Hasta hace no mucho, PHP no contaba con una buena forma de gestionar esas dependencias. Incluso cuando se gestionan manualmente, toca preocuparse por los autocargadores. Pero hoy por hoy eso ya no es un ningún problema.

Actualmente existen dos principales gestores de dependencias para PHP: Composer_ y PEAR_. Composer es por mucho el gestor de dependencias más popular para PHP, aunque por mucho tiempo PEAR fue el favorito de la comunidad. Conocer la historia de PEAR es buena idea, podrías encontrar referencias a PEAR incluso aunque no lo hayas usado nunca.

Composer y *Packagist*
======================

Composer es un gestor de dependencias *brillante*. Basta con listar las dependencias de un proyecto en el archivo ``composer.json`` y, con unos sencillos comandos, Composer automáticamnte descargará y configurará la autocarga de dependencias. Composer es el análogo de NPM en el mundo de ``node.js``, o de *Bundler* en el mundo Ruby.

Existe un montón de bibliotecas para PHP que son compatibles con Composer, listas para ser utilizadas en cualquier proyecto PHP. Esos «paquetes» se listan en Packagist_, el repositorio oficial de las bibliotecas PHP compatibles con Composer.

Cómo instalar Composer
----------------------

La mejor manera para descargar Composer es siguiendo las `instrucciones oficiales <https://getcomposer.org/download/>`_. Esto verificará que el instalador no esté corrupto o amañado.
El instalador instalará Composer *localmente* en el directorio actual de trabajo.

Se recominda instalarlo *globalmente* (por ejemplo, una copia única en ``/usr/local/bin``), para hacerlo (dependiendo de tu sistema) podrías ejecutar:

.. code-block:: console

  mv composer.phar /usr/local/bin/composer

.. note::
  Si lo anterior falla debido a permisos, anteponé el comando ``sudo``.

Para ejecutar Composer instalado localmente se debe invocar utilizando la llamada de PHP: ``php composer.phar``; y cuando está instaldo globalmente basta ``composer``.

Instarar en Windows
^^^^^^^^^^^^^^^^^^^

Para lo usuarios de Windows la manera más sencilla para instalar y ejecutar Composer es utilizar el instalador `ComposerSetup <https://getcomposer.org/Composer-Setup.exe>`_, que prepara una instalación global y configura el ``$PATH`` de Windows de tal manera que basta invocar el comando ``composer`` desde cualquier directorio en la CLI.

Cómo instalar composer manualmente
----------------------------------

La instalación manual de composer en una técnica avanzada...

PEAR
====

El gestor de dependencias veterano que muchos desarrolladores de PHP disfrutan es PEAR.

Cómo instalar PEAR
------------------

Para instalar PEAR

Cómo instalar un paquete
------------------------

Si un paquete

Manejando dependencias de PEAR con Composer
-------------------------------------------

Si se tiene

.. _Composer: https://getcomposer.org/
.. _PEAR: https://pear.php.net/
.. _Packagist: https://packagist.org/
