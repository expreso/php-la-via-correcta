Guía de estilo y las *PSR*
**************************

La comunidad de PHP es amplia y diversa, compuesta por una innumerable cantidad de bibliotecas, *frameworks* y componentes. Es común para los desarrolladores de PHP escoger varios de esos elementos y combinarlos en un sólo proyecto. Es importante que el código PHP se adhiera (tanto como sea posible) a un estilo de código común para facilitar a los desarrolladores el mezclar y empalmar varias bibliotecas en sus proyectos.

El `Framework Interop Group <http://www.php-fig.org/>`_ (Grupo de Iteroperatividad de *Frameworks*) ha propuesto y aprobado una serie de recomendaciones de estilo. No todas están relacionadas con *estilo de código*, pero aquellas que sí lo hacen son las PSR-0_, PSR-1_, PSR-2_ y PSR-4_. Dichas recomentaciones no son más que una serie de reglas que muchos proyectos como Drupal, Zend, Symfony, Laravel, CakePHP, phpBB, AWS SDK, FuelPHP, Lithium, etc,. han adoptado. Todo(a) desarrollador(a) puede utilizarlas en sus propios proyectos, pero no son ninguna imposición, también cada cual puede continuar con su propio estilo.

No obstante, idealmente, se debería escribir código PHP que se adhiera a convenciones conocidas (porque en ello radica la *interoperatividad* ). Esto puede ser cualquier combinación de PSR, o una de las convenciones definidas por PEAR o Zend. Esto significa que otros desarrolladores pueden leer y trabajar fácilmente con código de terceros, y aquellas aplicaciones que implementen los componentes podrán mantenerse consistentes incluso cuando trabajen con un amplio rango de código de terceros.

* Leé sobre PSR-0_
* Leé sobre PSR-1_
* Leé sobre PSR-2_
* Leé sobre PSR-4_
* `Leé sobre la convención de estilo de PEAR <http://pear.php.net/manual/en/standards.php>`_
* `Leé sobre la convención de estilo de Symfony <http://symfony.com/doc/current/contributing/code/standards.html>`_

Podés utilizar `PHP_CodeSniffer <http://pear.php.net/package/PHP_CodeSniffer/>`_ para verificar el código contra alguna de las recomendaciones listadas, y extensiones para editores de texto como `Atom <http://atom.io/>`_ para obtener retroalimentación en tiempo real.

Es posible corregir un esquema de código automáticamente utilizando una de las siguientes herramientas:

* `PHP Coding Standards Fixer <http://cs.sensiolabs.org/>`_ (Corrector de convenciones de código para PHP) que posee usa nabe de código muy bien probada.
* `PHP Code Beautifier and Fixer`_ (Corrector y embellecedor de código para PHP) herramienta que se incluye con *PHP_CodeSniffer* y que puede utilizarse para ajustar el código como corresponde.

Y puede ejecutarse el comando `phpcs` manualmente desde consola:

.. code-block:: console

  phpcs -sw --standard=PSR2 file.php

La herramienta mostrará los errores y como corregirlos.
También puede ser útil incluir este comando en un *hook* de git. así, ramas que contengan violaciones contra una convención particular no podrán entrar en el repositorio hasta que las misma sean corregidas.

Si se tiene *PHP_CodeSniffer*, entonces se puede corregir los problemas de esquema de código reportados, automáticamente, con `PHP Code Beautifier and Fixer`_.

.. code-block:: console

  php-cs-fixer fix -v --level=psr2 file.php

El inglés está predefinido para todos los nombres simbólicos y código de infraestructura. Los comentarios pueden estar escritos en cualquier idioma fácilmente legible por todas las personas actuales y futuras que puedan trabajar con el código.

.. _PSR-0: http://www.php-fig.org/psr/psr-0/
.. _PSR-1: http://www.php-fig.org/psr/psr-1/
.. _PSR-2: http://www.php-fig.org/psr/psr-2/
.. _PSR-4: http://www.php-fig.org/psr/psr-4/
.. _PHP Code Beautifier and Fixer: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Fixing-Errors-Automatically
